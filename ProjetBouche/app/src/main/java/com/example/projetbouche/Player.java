package com.example.projetbouche;

import android.content.Context;

public class Player extends Entity {
    private int health;

    Player (int power, int health, Context context){
        super(power, context);
        this.health = health;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }
}
