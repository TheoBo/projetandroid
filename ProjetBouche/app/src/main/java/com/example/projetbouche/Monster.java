package com.example.projetbouche;


import android.content.Context;
import android.content.res.Resources;

public class Monster extends Entity {

    private String name;
    private int imageId;
    //actual power is power multiplied by the current level
    Monster(int minPower, int maxPower, int level, Context context){
        int power = computePower(minPower, maxPower);
        setPower(power * level);

        //look at how strong the monster is and decide on its identity from that
        //For example, if the monster is in the lower 9th of the possible power values, it will be a spider
        if(power < minPower + ((maxPower - minPower)/9)){
            name = context.getString(R.string.spider);
            imageId = R.drawable.spider;
            return;
        }

        if(power < minPower + (2 *(maxPower - minPower)/9)){
            name = context.getString(R.string.bat);
            imageId = R.drawable.bat;
            return;
        }

        if(power < minPower + (3 *(maxPower - minPower)/9)){
            name = context.getString(R.string.slime);
            imageId = R.drawable.slime;
            return;
        }

        if(power < minPower + (4 *(maxPower - minPower)/9)){
            name = context.getString(R.string.zombie);
            imageId = R.drawable.zombie;
            return;
        }

        if(power < minPower + (5 *(maxPower - minPower)/9)){
            name = context.getString(R.string.xenomorph);
            imageId = R.drawable.xenomorph;
            return;
        }

        if(power < minPower + (6 *(maxPower - minPower)/9)){
            name = context.getString(R.string.dragon);
            imageId = R.drawable.bahamut;
            return;
        }

        if(power < minPower + (7 *(maxPower - minPower)/9)){
            name = context.getString(R.string.crab);
            imageId = R.drawable.crab;
            return;
        }

        if(power < minPower + (8 *(maxPower - minPower)/9)){
            name = context.getString(R.string.mecha);
            imageId = R.drawable.mecha;
            return;
        }

        else{
            name = context.getString(R.string.renaud);
            imageId = R.drawable.renaud;
        }
    }



    public String getName() {
        return name;
    }



    public int getImageId() {
        return imageId;
    }



    private int computePower(int minPower,int maxPower){
        return (int) Math.round(Math.random()*(maxPower - minPower)) + minPower;
    }


}
