package com.example.projetbouche;

import java.util.Date;

public class LeaderBoardEntry {

    private String name;
    private String level;
    private String visitedRooms;
    private String power;
    private String date;


    LeaderBoardEntry(String name, String level, String visitedRooms, String power, String date){
        this.name = name;
        this.level = level;
        this.visitedRooms = visitedRooms;
        this.power = power;
        this.date = date;
    }

    public String getName() {
        return name;
    }

    public String getLevel() {
        return level;
    }

    public String getVisitedRooms() {
        return visitedRooms;
    }

    public String getPower() {
        return power;
    }

    public String getDate() {
        return date;
    }

}
