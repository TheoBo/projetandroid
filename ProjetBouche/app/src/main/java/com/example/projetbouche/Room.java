package com.example.projetbouche;

import android.content.Context;
import android.os.Parcelable;

public class Room {

    public static final int MONSTER = 0;
    public static final int POTION = 1;
    public static final int POWER_CHARM = 2;

    private Context context;
    private int type;
    private Monster monster;
    private boolean clear;
    private boolean explored;

    public Room(int type, Context context){
        this.context = context;
        this.type = type;
        clear = false;
    }

    public Room(int type, Context context, Monster monster){
        this(type, context);

        this.monster = monster;
    }

    public int getType() {
        return type;
    }

    public Monster getMonster(){
        return monster;
    }

    public void clear(){
        clear = true;
    }

    public void explore() {
        explored = true;
    }

    public boolean isClear(){
        return clear;
    }

    public boolean isExplored() {
        return explored;
    }
}
