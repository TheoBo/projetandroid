package com.example.projetbouche;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class Fight extends AppCompatActivity {

    public final static  int FLEE = 0;
    public final static  int LOSE = 1;
    public final static  int WIN = 2;

    public final static  String RESULT = "result";
    public final static  String ROOM = "room";

    private int roomNumber;
    private int playerPower;
    private int monsterPower;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fight);

        //get the intent and set the text and variables according to what was transmitted
        Intent intent = getIntent();
        String[] data = intent.getStringExtra(MainActivity.KEY).split(";");

        TextView hp = findViewById(R.id.player_hp);
        hp.setText(data[0]);

        playerPower = Integer.parseInt(data[1]);
        TextView pPower = findViewById(R.id.player_power);
        pPower.setText(data[1]);

        roomNumber = Integer.parseInt(data[2]);

        monsterPower = Integer.parseInt(data[3]);
        TextView mPower = findViewById(R.id.monster_power);
        mPower.setText(data[3]);

        String monsterName = data[4];
        TextView nameView = findViewById(R.id.monster_name);
        nameView.setText(monsterName);

        int imageId = Integer.parseInt(data[5]);
        ImageView portrait = findViewById(R.id.monster_portrait);
        portrait.setImageResource(imageId);

    }

    public void fight(View myButton){
        if(Math.random() * playerPower - Math.random() * monsterPower < 0){
            sendResult(LOSE);
        }
        else {
            sendResult(WIN);
        }
    }

    public void flee(View myButton){
        sendResult(FLEE);
    }

    private void sendResult(int result){
        Intent intent = new Intent();
        intent.putExtra(RESULT, result);
        intent.putExtra(ROOM, roomNumber);
        setResult(RESULT_OK, intent);
        finish();
    }

   @Override
    public void onBackPressed() {
        sendResult(FLEE);
    }
}
