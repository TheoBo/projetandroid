package com.example.projetbouche;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

public class LeaderboardListAdapter extends ArrayAdapter<String> {

    private final Activity context;
    private ArrayList<LeaderBoardEntry> entries;


    public LeaderboardListAdapter(Activity context, ArrayList<LeaderBoardEntry> entries, ArrayList<String> items){
        super(context, R.layout.leaderboard_list_view, items);
        this.context = context;
        this.entries = entries;
    }


    public View getView(int position, View view, ViewGroup parent){
        LayoutInflater inflater = context.getLayoutInflater();
        View rowView = inflater.inflate(R.layout.leaderboard_list_view, null, true);

        TextView rankText = rowView.findViewById(R.id.rank);
        TextView nameText = rowView.findViewById(R.id.name);
        TextView levelText = rowView.findViewById(R.id.level_list);
        TextView visitedRoomsText = rowView.findViewById(R.id.visited_rooms);
        TextView powerText = rowView.findViewById(R.id.power_list);
        TextView dateText = rowView.findViewById(R.id.date);

        if(position < entries.size()) {
            LeaderBoardEntry currentEntry = entries.get(position);
            String rank = String.valueOf(position + 1);

            rankText.setText(rank);
            nameText.setText(currentEntry.getName());
            levelText.setText(currentEntry.getLevel());
            visitedRoomsText.setText(currentEntry.getVisitedRooms());
            powerText.setText(currentEntry.getPower());
            dateText.setText(currentEntry.getDate());
        }

        return rowView;
    }
}
