package com.example.projetbouche;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.ViewCompat;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    public static final String KEY = "data";
    private static final int REQUEST_CODE = 7000;
    //id of the level up button
    private int levelUpId;
    private int unexploredRooms = 16;
    private int level = 1;
    private int totalBeatenRooms = 0;

    private int initialHP = 10;
    private int initialPower = 100;
    private int minMonsterPower = 1;
    private int maxMonsterPower = 150;

    private Player player;
    private ArrayList<Room> rooms;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initPlayer();
        initRooms();
        refreshText();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main,menu);

        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item){
        int id = item.getItemId();

        switch (id){
            case R.id.restart :
                reset();
                break;

            case R.id.quit :
                finish();
                break;

            case R.id.parameters :
                openParametersDialog();
                break;

            case R.id.leaderboard :
                openLeaderboard();
                break;

        }

        return true;
    }


    private void initPlayer(){
        player = new Player (initialPower, initialHP, this);
    }



    private void initRooms(){
        rooms = new ArrayList<>();
        rooms.add(new Room(Room.POTION, this));
        rooms.add(new Room(Room.POWER_CHARM, this));
        for(int i = 2; i < 16; ++i){
            rooms.add(new Room(Room.MONSTER, this, new Monster(minMonsterPower, maxMonsterPower, level, this)));
        }
        Collections.shuffle(rooms);
    }



    public void enterRoom(View myButton){
        if(player.getHealth() <= 0 || unexploredRooms == 0){
            return;
        }

        int roomNumber = (int) Long.parseLong(myButton.getTag().toString());
        Room room = rooms.get(roomNumber - 1);

        if(room.isClear()){
            setStatusText(getString(R.string.empty_room));
            setResultText("");
            return;
        }

        if(!room.isExplored()){
            room.explore();
        }

        if(room.getType() == Room.MONSTER) {
            String toSend = player.getHealth() + ";" + player.getPower() + ";" + roomNumber  + ";" + room.getMonster().getPower() + ";" + room.getMonster().getName() +";" + room.getMonster().getImageId();
            Intent intent = new Intent(this, Fight.class);
            intent.putExtra(KEY, toSend);
            startActivityForResult(intent,REQUEST_CODE);
            return;
        }

        if(room.getType() == Room.POTION){
            int healthIncrease = (int) (Math.round(Math.random() * 2.0) + 1);
            String message = getString(R.string.health_potion) + " : +" + healthIncrease + " " + getString(R.string.hp);
            setStatusText("");
            setResultText(message);
            player.setHealth(player.getHealth() + healthIncrease);
            clearRoom(roomNumber);
            updateButtonImage(roomNumber);
            refreshText();
            return;
        }

        if(room.getType() == Room.POWER_CHARM){
            int powerIncrease = (int) (Math.round(Math.random() * 5.0) + 5);
            String message = getString(R.string.power_charm) + " : +" + powerIncrease + " " + getString(R.string.power);
            setStatusText("");
            setResultText(message);
            player.setPower(player.getPower() + powerIncrease);
            clearRoom(roomNumber);
            updateButtonImage(roomNumber);
            refreshText();
        }
    }



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data){
        if(requestCode == REQUEST_CODE){
            setStatusText(getString(R.string.battle_result));
            int roomNumber = data.getIntExtra(Fight.ROOM, -1);
            int result = data.getIntExtra(Fight.RESULT, Fight.FLEE);
            if(result == Fight.LOSE){
                defeat();
            }
            else {
                if(result == Fight.WIN){
                    victory(roomNumber);
                }
                else{
                    flee();
                }
            }
            updateButtonImage(roomNumber);
            refreshText();
        }
    }



    private void setResultText(String text){
        TextView textView = findViewById(R.id.result);
        textView.setText(text);
    }



    private void setStatusText(String text){
        TextView textView = findViewById(R.id.status);
        textView.setText(text);
    }



    private void defeat(){
        player.setHealth(Math.max(player.getHealth() - 3, 0));
        setResultText(getString(R.string.battle_lose));
        if(player.getHealth() <= 0){
            setStatusText(getString(R.string.game_lose));
        }
    }



    private void flee(){
        player.setHealth(Math.max(player.getHealth() - 1, 0));
        setResultText(getString(R.string.battle_flee));
        if(player.getHealth() <= 0){
            setStatusText(getString(R.string.game_lose));
        }
    }



    private void victory(int roomNumber){
        player.setPower(player.getPower() + 10);
        setResultText(getString(R.string.battle_win));
        clearRoom(roomNumber);
    }



    private void clearRoom(int roomNumber){
        rooms.get(roomNumber - 1).clear();
        unexploredRooms--;
        totalBeatenRooms++;
        if(unexploredRooms == 0){
            setStatusText(getString(R.string.game_win));
            addLevelUpButton();
        }
    }

    private void addLevelUpButton(){
        LinearLayout layout = findViewById(R.id.main);

        final Button levelUp = new Button(this);
        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        params.gravity = Gravity.CENTER;
        levelUp.setLayoutParams(params);
        levelUp.setGravity(Gravity.CENTER);
        levelUp.setText(getString(R.string.level_up));
        levelUpId = ViewCompat.generateViewId();
        levelUp.setId(levelUpId);

        levelUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                increaseLevel(levelUp);
            }
        });

        layout.addView(levelUp);
    }



    //update the image of the button so it matches the room state
    private void updateButtonImage(int roomNumber){
        int id = getResources().getIdentifier("room" + roomNumber, "id", getPackageName());
        ImageButton button = findViewById(id);
        if(rooms.get(roomNumber - 1).isClear()){
            button.setImageResource(R.drawable.red_cross);
            return;
        }

        if(rooms.get(roomNumber - 1).isExplored()){
            button.setImageResource(R.drawable.skull);
            return;
        }

        button.setImageResource(R.drawable.question_mark);
    }



    private void refreshText(){
        TextView unexplored = findViewById(R.id.unexplored);
        unexplored.setText(String.valueOf(unexploredRooms));

        TextView power = findViewById(R.id.power);
        power.setText(String.valueOf(player.getPower()));

        TextView hp = findViewById(R.id.hp);
        hp.setText(String.valueOf(player.getHealth()));

        TextView levelView = findViewById(R.id.level);
        String levelText = getString(R.string.level) + " " + level;
        levelView.setText(levelText);
    }



    private void increaseLevel(View myButton){
        level++;
        resetRoomsAndText();
    }



    private void reset(){
        updateLeaderboard();
        initPlayer();
        level = 1;
        totalBeatenRooms = 0;
       resetRoomsAndText();
    }



    private void resetRoomsAndText(){
        initRooms();
        //reset buttons' text
        for(int i = 0; i < rooms.size(); ++i){
            updateButtonImage(i+1);
        }

        //remove level up button if present
        if(unexploredRooms == 0) {
            Button levelUp = findViewById(levelUpId);
            LinearLayout layout = findViewById(R.id.main);
            layout.removeView(levelUp);
        }

        unexploredRooms = 16;
        refreshText();
        setStatusText(getString(R.string.battle_result));
        setResultText("");
    }

    private void openParametersDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View layout = getLayoutInflater().inflate(R.layout.dialog_parameters, null);
        builder.setView(layout);
        builder.setPositiveButton(getString(R.string.accept), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {}
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();

        alertDialog.getButton(alertDialog.BUTTON_POSITIVE).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String initialHPString = ((EditText)layout.findViewById(R.id.initial_hp)).getText().toString();
                String initialPowerString = ((EditText)layout.findViewById(R.id.initial_power)).getText().toString();
                String minMonsterPowerString = ((EditText)layout.findViewById(R.id.min_monster_power)).getText().toString();
                String maxMonsterPowerString = ((EditText)layout.findViewById(R.id.max_monster_power)).getText().toString();

                if(initialHPString.equals("") || initialPowerString.equals("") || minMonsterPowerString.equals("") || maxMonsterPowerString.equals("")){
                    TextView error = layout.findViewById(R.id.error);
                    error.setText(getString(R.string.parameters_error_message));
                    return;
                }

                //todo: disallow 0 values
                int initHP = Integer.parseInt(initialHPString);
                int initPower = Integer.parseInt(initialPowerString);
                int minMonsterPow = Integer.parseInt(minMonsterPowerString);
                int maxMonsterPow = Integer.parseInt(maxMonsterPowerString);

                if(initHP <= 0 || initPower <= 0 || minMonsterPow <= 0 || maxMonsterPow <= 0){
                    TextView error = layout.findViewById(R.id.error);
                    error.setText(getString(R.string.zero_error));
                    return;
                }

                if(minMonsterPow > maxMonsterPow){
                    TextView error = layout.findViewById(R.id.error);
                    error.setText(getString(R.string.min_max_error));
                    return;
                }

                initialHP = initHP;
                initialPower = initPower;
                minMonsterPower = minMonsterPow;
                maxMonsterPower = maxMonsterPow;
                reset();
                alertDialog.dismiss();
            }
        });
    }



    private void openLeaderboard(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        final View layout = getLayoutInflater().inflate(R.layout.leaderboard, null);
        builder.setView(layout);
        ListView ranking = layout.findViewById(R.id.ranking);
        ArrayList<String> items = new ArrayList<>();
        for(int i = 0; i < 10; ++i){
            items.add( String.valueOf(i + i));
        }
        ranking.setAdapter(new LeaderboardListAdapter(this, loadFile(), items));

        builder.setPositiveButton(getString(R.string.close), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }



    private void updateLeaderboard(){
        ArrayList<LeaderBoardEntry> entries = loadFile();
        boolean added = false;
        int i = 0;
        //Players are ranked based on the number of rooms they cleared
        while (i < entries.size() && !added) {
            //compare the amount of visited rooms with the current leaderboard
            int visitedRooms = Integer.parseInt(entries.get(i).getVisitedRooms());
            if(totalBeatenRooms >= visitedRooms){
                //check if the player wants to be added
                    LeaderBoardEntry newEntry = generateLeaderboardEntry();
                    entries.add(i, newEntry);

                    //check if the new entry push the current number of entries above 10 and delete the last one in that case
                    if (entries.size() > 10) {
                        entries.remove(10);
                    }

                    save(entries);

                    added = true;
            }
            ++i;
        }
        //if the entry is last but there's still room to add it
        if(!added && entries.size() < 10){
            LeaderBoardEntry newEntry = generateLeaderboardEntry();
            entries.add(newEntry);
            save(entries);
        }
    }

    private String readFile(){
        String ret = "";

        try{
            InputStream inputStream = this.openFileInput("leaderboard.txt");

            if(inputStream != null){
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receivedString = "";
                StringBuilder stringBuilder = new StringBuilder();
                while ((receivedString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receivedString + "\n");
                }
                ret = stringBuilder.toString();
                inputStream.close();
            }

        }catch (Exception e) {
            e.printStackTrace();
        }
        return ret;
    }

    private void writeToFile(String data){
        try{
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(this.openFileOutput("leaderboard.txt",this.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<LeaderBoardEntry> loadFile(){
        String items = readFile();
        ArrayList<LeaderBoardEntry> entries = new ArrayList<>();

        if(!items.equals("")) {
            String[] itemList = items.split("\n");
            for (int i = 0; i < itemList.length; ++i) {
                //Could use a better separator here
                String[] currentEntry = itemList[i].split(";");
                entries.add(new LeaderBoardEntry(currentEntry[0], currentEntry[1], currentEntry[2], currentEntry[3], currentEntry[4]));
            }
        }
        return  entries;
    }

    private void save(ArrayList<LeaderBoardEntry> entries){
        String toWrite = "";
        for(int i = 0; i < entries.size(); ++i){
            LeaderBoardEntry currentEntry = entries.get(i);
            toWrite += currentEntry.getName() + ";" + currentEntry.getLevel() + ";" + currentEntry.getVisitedRooms() + ";" + currentEntry.getPower() + ";" + currentEntry.getDate() + "\n";
        }
        writeToFile(toWrite);
    }


    private LeaderBoardEntry generateLeaderboardEntry(){

        Date currentDate = Calendar.getInstance().getTime();
        DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
        return new LeaderBoardEntry("player", String.valueOf(level), String.valueOf(totalBeatenRooms), String.valueOf(player.getPower()), df.format(currentDate));
    }
}
