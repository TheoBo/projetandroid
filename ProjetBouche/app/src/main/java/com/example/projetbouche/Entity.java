package com.example.projetbouche;

import android.content.Context;

public class Entity {
    private int power;

    private Context context;

    public Entity(){

    }

    public Entity(int power, Context context){
        this.power = power;
        this.context = context;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power){
        this.power = power;
    }
}
